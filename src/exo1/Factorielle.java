package exo1;
import java.math.*;



public class Factorielle {

	private int facto = 1;
	private double dfacto = 1;

	
	public int intFactorielle(int valeur)
	{
		int i;
		for(i=1;i<valeur;i++)
		{
			facto = facto*i;
			
		}
		
		return facto;
	}
	
	
	public double doubleFactorielle (int valeur)
	{
		int i;
		for(i=valeur; i>0;i--)
		{
			dfacto = dfacto*i;
			
		}
		
		return dfacto;
	}
		public BigInteger bigIntegerFactorielle (int valeur)
		{
			//int i;
			BigInteger bifacto = BigInteger.ONE ;
			for(int i=valeur; i>0;i--)
			{
				String s = ""+i;
				BigInteger I = new BigInteger(s);
				bifacto = bifacto.multiply(I);
				
			}
			

			return bifacto;
		
		
	}
	
}


