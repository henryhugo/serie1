package exo1;

public class Main {

	public static void main(String[] args) {
			Factorielle fact = new Factorielle();
			System.out.println("Factorielle de 10 = " + fact.intFactorielle(10));
			System.out.println("Factorielle de 10 (double) = " + fact.doubleFactorielle(10));
			System.out.println("Factorielle de 10 (bigInteger) = " + fact.bigIntegerFactorielle(10));
		}

}
